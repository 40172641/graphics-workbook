#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtc\constants.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\euler_angles.hpp>
#include <glm\gtx\projection.hpp>

using namespace std;
using namespace glm;

int main()
{
	//Vec2 Variables
	vec2 u(1.0f, 0.0f);
	vec2 y(1.0f, 0.0f);

	//Vec3 Variables
	vec3 v(1.0f, 1.0f, 0.0f);
	vec3 z(-1.0f, 1.0f, 0.0f);

	//Vec4 Variables
	vec4 w(1.0f, 0.0f, 0.0f, 0.0f);
	vec4 ww(1.0f, 0.0f, 0.0f, 0.0f);

	//Converting Vectors

	vec2 convertU(vec3(1.0f, 1.0f, 0.0f)); // Converts to a 3D Vector;
	vec2 convertUU(vec4(1.0f, 0.0f, 0.0f, 0.0f)); // Converts to a 4D Vector

	//Vec3 Variables
	//vec3 convertY(vec2(1.0f,0.0f));
	//vec3 convertYY(vec4(-1.0f, 1.0f, 0.0f, 0.0f));

	//Vec4 Variables
	//vec4 convertZ(vec2(1.0f, 0.0f, 0.0f, 0.0f));
	//vec4 convertZZ(vec3(1.0f, 0.0f, 0.0f));

	//Can access individual parts of the vector by getting or setting
	float x = u.x;
	u.y = 10.0f;
	// Can use x, y, z, w or r, g, b, a

	//Addition/Subtraction of Vectors
	vec3 save = v + z;
	vec2 example = u + y;
	vec2 example1 = example - y;

	//Scaling - The amount we scale by multiplying floating point numbers
	vec3 v = 5.0f + v;
	vec3 z = v / 5.0f;

	//Length of a Vector - Finds the Length of a Vector
	float l = length(y);
	float d = length(z);
	float x = length(w);

	//Normalising a Vector - Will Return a Normalised Version of the Vector
	vec3 n = normalize(z); // Normalises Vector z 

	//Dot Product - Finds the Dot Product of the two Vectors
	float d = dot(v, z);

	// Vector Projection
	vec3 p = proj(v, z); // Calculates the projection of one Vector onto another

	//Cross Product - Calculates the cross product of Two Vectors
	vec3 c = cross(v, z);

	//Matrices
	mat4 m(1.0f); //Identfies the Matrix

	mat3 n(mat4(1.0f));
	//Last Column and Row are Dropped
	mat3 k(0.0f);
	mat4 q(1.0f);
	mat4 mat4z(0.0f);

	//Matrix Addition
	mat4 add = m + mat4z; // Adds two matrices
	mat4 sub = m - mat4z; // Subtracts two matrices

	//Matrix Scaling 
	mat4 m = 5.0f * mat4z; //Scales the Vector by the Floating Point
	mat4 o = m / 5.0f; // Scales the vector down by thr Floating Point

	//Matrix Multiplication
	mat4 m = q * mat4z; // Multiples the Two Matrices together 

	//Matrix-Vector Multiplication
	mat4 T;
	vec3 z;
	//Converts to a 4D Vector
	vec4 v = T * vec4(z, 1.0f);
	//Can always get a 3D Version aswell
	vec3 w = vec3(T * vec4(z, 1.0f));

	//Transformations Matrix
	mat4 T = translate(mat4(1.0f), vec3(1.0f, 0.0f, 0.0f));

	//Search google for autos window https://msdn.microsoft.com/en-us/library/aa290702(v=vs.71).aspx
}