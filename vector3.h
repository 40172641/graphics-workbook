#pragma once

// Minimal vector3 class

#pragma once


class vector3
{
public:
	float x, y, z;

	vector3();
	vector3(float x, float y, float z);
	vector3(const vector3& rhs);

	void	operator+= (const vector3 &v);
	void	operator-= (const vector3 &v);
	void	operator*= (const float    s);
	vector3 operator/  (const float    s) const;
	vector3 operator-  (const vector3 &v) const;
	vector3 operator+  (const vector3 &v) const;
	vector3 operator*  (const float    s) const;
	vector3 operator-  ()				  const; // Unary minus

	static vector3  Cross(const vector3 &vA, const vector3& vB);
	static float	Dot(const vector3 &vA, const vector3& vB);
	static vector3  Normalize(const vector3& v);
	static float	Length(const vector3& v);
	static float	LengthSq(const vector3& v);

}; // End vector3(..)
